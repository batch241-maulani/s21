

let wrestlers = ["Dwayne Johnson","Steve Austin", "Kurt Angle", "Dave Bautista"]

console.log("Original Array:")
console.log(wrestlers)

function addArray(value){
	wrestlers[wrestlers.length++] = value
}

addArray("John Cena")

console.log(wrestlers)


let itemFound


function retrieveItem(index){
	return wrestlers[index]
}

itemFound = retrieveItem(2)

console.log(itemFound)


function deleteItem(){
	let temp = wrestlers[wrestlers.length-1]
	delete wrestlers.pop(wrestlers.length-1)
	return temp
}

let deletedItem = deleteItem()

console.log(deletedItem)

console.log(wrestlers)

function updateArray(index, element){
	wrestlers[index] = element
}

updateArray(3,"Triple H")

console.log(wrestlers)


function deleteAllItem(){
	wrestlers.length = 0
}

deleteAllItem()

console.log(wrestlers)

let isUserEmpty

function checkifEmpty(){
	return !wrestlers.length>0
}

isUserEmpty = checkifEmpty()

console.log(isUserEmpty)